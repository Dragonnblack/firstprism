﻿using System.Windows.Input;
using FirstPrism.Views;
using Prism.Commands;
using Prism.Modularity;
using Prism.Mvvm;
using Prism.Navigation;

namespace FirstPrism.ViewModels
{
    public class Page2ViewModel : BindableBase, INavigationAware
    {
        private readonly INavigationService _navigationService;
        public DelegateCommand NavigateToFirstCommand { get; set; }

        public Page2ViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;
            NavigateToFirstCommand = new DelegateCommand(NavigateToFirst);
        }

        private string _title;
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }


        private void NavigateToFirst()
        {
            _navigationService.NavigateAsync($"{nameof(FirstPrismPage)}?text=Hello back from second page");
        }

        public void OnNavigatedFrom(NavigationParameters parameters)
        {
            // Called when the implementer has been navigated away from.
        }

        public void OnNavigatedTo(NavigationParameters parameters)
        {
            // Called when the implementer has been navigated to.
            if (parameters.ContainsKey("text"))
                Title = (string)parameters["text"] + " and Prism";
        }

        public void OnNavigatingTo(NavigationParameters parameters)
        {
            //Called before the implementor has been navigated to - but not called when using 
            // device hardware or software back buttons.
        }
    }
}