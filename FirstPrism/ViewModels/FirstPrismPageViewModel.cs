﻿using System.Windows.Input;
using FirstPrism.Views;
using Prism.Commands;
using Prism.Modularity;
using Prism.Mvvm;
using Prism.Navigation;

namespace FirstPrism.ViewModels
{
    public class FirstPrismPageViewModel : BindableBase, INavigationAware
    {
        private readonly INavigationService _navigationService;
        public DelegateCommand NavigateToSamplePageCommand { get; set; }

        public FirstPrismPageViewModel(INavigationService navigationService)
        {
            _navigationService = navigationService;
            NavigateToSamplePageCommand = new DelegateCommand(NavigateToSamplePage);
        }

        private string _title;
        public string Title
        {
            get { return _title; }
            set { SetProperty(ref _title, value); }
        }

        private void NavigateToSamplePage()
        {
            _navigationService.NavigateAsync($"{nameof(Page2)}?text=Hello from the first page");
        }

        public void OnNavigatedFrom(NavigationParameters parameters)
        {
            // Called when the implementer has been navigated away from.
        }

        public void OnNavigatedTo(NavigationParameters parameters)
        {
            // Called when the implementer has been navigated to.
            if (parameters.ContainsKey("text"))
                Title = (string)parameters["text"] + ", Awesome.";
        }

        public void OnNavigatingTo(NavigationParameters parameters)
        {
            //Called before the implementor has been navigated to - but not called when using 
            // device hardware or software back buttons.
        }
    }
}