﻿using Prism;
using Prism.Ioc;
using Prism.Unity;
using Xamarin.Forms;
using FirstPrism.Views;
using FirstPrism.ViewModels;

namespace FirstPrism
{
    public partial class App : PrismApplication
    {
        public App(IPlatformInitializer initializer = null) : base(initializer) { }

        protected override void OnInitialized()
        {
            InitializeComponent();

            NavigationService.NavigateAsync(nameof(FirstPrismPage));
        }

        protected override void RegisterTypes(Prism.Ioc.IContainerRegistry containerRegistry) 
        {
            containerRegistry.RegisterForNavigation<FirstPrismPage, FirstPrismPageViewModel>();
            containerRegistry.RegisterForNavigation<Page2, Page2ViewModel>();
        
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
